//package com;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import model.Impiegato;
//import model.Ruolo;
//import model.Storico;
//
//public class Gestionale {
//
//	private List<Impiegato> impiegati;
//	private List<Ruolo> ruoli;
//	private List<Storico> storico;
//
//	public Gestionale() {
//		this.impiegati = new ArrayList<>();
//		this.ruoli = new ArrayList<>();
//		this.storico = new ArrayList<>();
//	}
//
//	// AGGIUNGO UN IMPIEGATO
//	public void aggImpiegato(String nome, String cognome, Integer matricola, String email) {
//		Impiegato impiegato = null;
//		for (Impiegato i : this.impiegati) {
//			if (i.getMatricola().equals(matricola))
//				impiegato = i;
//		}
//		if (impiegato == null) {
//			impiegato = new Impiegato(nome, cognome, matricola, email);
//			this.impiegati.add(impiegato);
//			System.out.println("Impiegato inserito");
//		} else
//			System.out.println("Esiste gi� un impiegato con questa matricola");
//
//	}
//
//	// AGGIUNGO UN RUOLO
//	public void aggRuolo(String idRuolo, Integer stipendio) {
//		/*
//		 * Ruolo ruolo = null; for (Ruolo r : this.ruoli) {
//		 * if(r.getIdRuolo().equals(idRuolo)) ruolo = r; } if(ruolo == null) { ruolo =
//		 * new Ruolo(idRuolo, stipendio); this.ruoli.add(ruolo);
//		 * System.out.println("Ruolo inserito"); }else
//		 * System.out.println("Esiste gi� questo ruolo");
//		 */
//	}
//
//	// MODIFICO IL NOME DELL'IMPIEGATO
//	public void modificaNomeImpiegato(Integer matricola, String nuovoNome) {
//		Impiegato impiegato = null;
//		for (Impiegato i : this.impiegati) {
//			if (i.getMatricola().equals(matricola))
//				impiegato = i;
//		}
//		if (impiegato != null) {
//			impiegato.setNome(nuovoNome);
//			System.out.println("Impiegato modificato!");
//		} else
//			System.out.println("L'impiegato non esiste!");
//	}
//
//	// MODIFICO IL COGNOME DELL'IMPIEGATO
//	public void modificaCognomeImpiegato(Integer matricola, String nuovoCognome) {
//		Impiegato impiegato = null;
//		for (Impiegato i : this.impiegati) {
//			if (i.getMatricola().equals(matricola))
//				impiegato = i;
//		}
//		if (impiegato != null) {
//			impiegato.setCognome(nuovoCognome);
//			System.out.println("Impiegato modificato!");
//		} else
//			System.out.println("L'impiegato non esiste!");
//	}
//
//	// MODIFICO L'EMAIL DELL'IMPIEGATO
//	public void modificaEmailImpiegato(Integer matricola, String nuovaEmail) {
//		Impiegato impiegato = null;
//		for (Impiegato i : this.impiegati) {
//			if (i.getMatricola().equals(matricola))
//				impiegato = i;
//		}
//		if (impiegato != null) {
//			impiegato.setEmail(nuovaEmail);
//			System.out.println("Impiegato modificato!");
//		} else
//			System.out.println("L'impiegato non esiste!");
//	}
//
//	// MODIFICO LO STIPENDIO DEL RUOLO
//	public void modificaStipendioRuolo(String idRuolo, Integer nuovoStipendio) {
//		Ruolo ruolo = null;
//		for (Ruolo r : this.ruoli) {
//			if (r.getIdRuolo().equals(idRuolo))
//				ruolo = r;
//		}
//		if (ruolo != null) {
//			ruolo.setStipendio(nuovoStipendio);
//			System.out.println("Ruolo modificato!");
//		} else
//			System.out.println("Il ruolo non esiste!");
//	}
//
//	// ASSEGNO UN RUOLO AD UN IMPIEGATO
//	public void assegnaRuolo(Integer idRuolo, Integer matricolaImpiegato, String dataInizioS, String dataFineS)
//			throws ParseException {
//
//		Date dataInizio = new SimpleDateFormat("dd/MM/yyyy").parse(dataInizioS);
//		Date dataFine = new SimpleDateFormat("dd/MM/yyyy").parse(dataFineS);
//
//		Storico storico = new Storico();
//
//		this.storico.add(storico);
//		System.out.println("Ruolo assegnato");
//	}
//
//	// CERCO UN IMPIEGATO
//	public void ricercaImpiegato(Integer matricola) {
//		Impiegato impiegato = null;
//		for (Impiegato i : this.impiegati) {
//			if (i.getMatricola().equals(matricola))
//				impiegato = i;
//		}
//		if (impiegato != null) {
//			System.out.println("Impiegato : ");
//			System.out.println(" - Nome : " + impiegato.getNome());
//			System.out.println(" - Cognome : " + impiegato.getCognome());
//			System.out.println(" - Matricola : " + impiegato.getMatricola().toString());
//			System.out.println(" - Email : " + impiegato.getEmail());
//		}
//	}
//
//	// CERCO GLI IMPIEGATI CHE SVOLGONO UN DETERMINATO RUOLO CON DATA DI INIZIO E
//	// FINE
//	public void ricercaImpiegatoPerRuolo(String idRuolo, String dataInizioS, String dataFineS) throws ParseException {
//		List<Impiegato> impiegatiTrovati = new ArrayList<>();
//
//		Date dataInizio = new SimpleDateFormat("dd/MM/yyyy").parse(dataInizioS);
//		Date dataFine = new SimpleDateFormat("dd/MM/yyyy").parse(dataFineS);
//
//		for (Storico s : this.storico) {
//			if (s.getIdRuolo().equals(idRuolo) && s.getDataInizio().after(dataInizio)
//					&& s.getDataFine().before(dataFine) && !dataInizio.after(s.getDataInizio())
//					&& !dataFine.before(s.getDataFine())) {
//				for (Impiegato i : this.impiegati) {
//					if (i.getMatricola().equals(s.getMatricola()))
//						impiegatiTrovati.add(i);
//				}
//			}
//		}
//		if (impiegatiTrovati != null && !impiegatiTrovati.isEmpty()) {
//			System.out.println("Trovati");
//			for (Impiegato impiegato : impiegatiTrovati) {
//				System.out.println("Impiegato : ");
//				System.out.println(" - Nome : " + impiegato.getNome());
//				System.out.println(" - Cognome : " + impiegato.getCognome());
//				System.out.println(" - Matricola : " + impiegato.getMatricola().toString());
//				System.out.println(" - Email : " + impiegato.getEmail());
//			}
//		} else
//			System.out.println("Nessun impiegato trovato");
//	}
//}
