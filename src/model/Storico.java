package model;

import java.util.Date;

public class Storico {
	private Integer idStorico;
	private Integer idRuolo;
	private Integer matricola;
	private Date dataInizio;
	private Date dataFine;

	public Storico() {
	}

	public Storico(Integer idStorico, Integer idRuolo, Integer matricola, Date dataInizio, Date dataFine) {
		super();
		this.idStorico = idStorico;
		this.idRuolo = idRuolo;
		this.matricola = matricola;
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
	}

	public Integer getIdStorico() {
		return this.idStorico;
	}

	public void setIdStorico(Integer idStorico) {
		this.idStorico = idStorico;
	}

	public Integer getIdRuolo() {
		return this.idRuolo;
	}

	public void setIdRuolo(Integer idRuolo) {
		this.idRuolo = idRuolo;
	}

	public Integer getMatricola() {
		return this.matricola;
	}

	public void setMatricola(Integer matricola) {
		this.matricola = matricola;
	}

	public Date getDataInizio() {
		return this.dataInizio;
	}

	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	public Date getDataFine() {
		return this.dataFine;
	}

	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}
}
