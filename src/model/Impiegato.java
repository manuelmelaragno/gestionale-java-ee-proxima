package model;

public class Impiegato {

	private String nome;
	private String cognome;
	private Integer matricola;
	private String email;

	public Impiegato() {
	}

	public Impiegato(String nome, String cognome, Integer matricola, String email) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.matricola = matricola;
		this.email = email;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return this.cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public Integer getMatricola() {
		return this.matricola;
	}

	public void setMatricola(Integer matricola) {
		this.matricola = matricola;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
