package model;

public class Ruolo {

	private Integer idRuolo;
	private Integer stipendio;
	private String descrizione;

	public Ruolo() {
	}

	public Ruolo(Integer idRuolo, Integer stipendio) {
		super();
		this.idRuolo = idRuolo;
		this.stipendio = stipendio;
	}

	public Integer getIdRuolo() {
		return this.idRuolo;
	}

	public void setIdRuolo(Integer idRuolo) {
		this.idRuolo = idRuolo;
	}

	public Integer getStipendio() {
		return this.stipendio;
	}

	public void setStipendio(Integer stipendio) {
		this.stipendio = stipendio;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
}
