/**
 * CorsoService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public interface CorsoService extends java.rmi.Remote {
    public ws.ResponseCorsoSelectById getCorsoById(java.lang.Integer arg0) throws java.rmi.RemoteException;
    public ws.ResponseCorsoDelete deleteCorso(java.lang.Integer arg0) throws java.rmi.RemoteException;
    public ws.ResponseCorsoUpdate updateCorsoNome(java.lang.Integer arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public ws.ResponseCorsoInsert insertCorso(java.lang.String arg0) throws java.rmi.RemoteException;
    public ws.ResponseCorsoSelectAll getAllCorsi() throws java.rmi.RemoteException;
}
