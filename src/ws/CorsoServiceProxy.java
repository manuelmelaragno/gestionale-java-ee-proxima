package ws;

public class CorsoServiceProxy implements ws.CorsoService {
  private String _endpoint = null;
  private ws.CorsoService corsoService = null;
  
  public CorsoServiceProxy() {
    _initCorsoServiceProxy();
  }
  
  public CorsoServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initCorsoServiceProxy();
  }
  
  private void _initCorsoServiceProxy() {
    try {
      corsoService = (new ws.CorsoServiceServiceLocator()).getCorsoServicePort();
      if (corsoService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)corsoService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)corsoService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (corsoService != null)
      ((javax.xml.rpc.Stub)corsoService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ws.CorsoService getCorsoService() {
    if (corsoService == null)
      _initCorsoServiceProxy();
    return corsoService;
  }
  
  public ws.ResponseCorsoDelete deleteCorso(java.lang.Integer arg0) throws java.rmi.RemoteException{
    if (corsoService == null)
      _initCorsoServiceProxy();
    return corsoService.deleteCorso(arg0);
  }
  
  public ws.ResponseCorsoUpdate updateCorsoNome(java.lang.Integer arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (corsoService == null)
      _initCorsoServiceProxy();
    return corsoService.updateCorsoNome(arg0, arg1);
  }
  
  public ws.ResponseCorsoInsert insertCorso(java.lang.String arg0) throws java.rmi.RemoteException{
    if (corsoService == null)
      _initCorsoServiceProxy();
    return corsoService.insertCorso(arg0);
  }
  
  public ws.ResponseCorsoSelectAll getAllCorsi() throws java.rmi.RemoteException{
    if (corsoService == null)
      _initCorsoServiceProxy();
    return corsoService.getAllCorsi();
  }
  
  public ws.ResponseCorsoSelectById getCorsoById(java.lang.Integer arg0) throws java.rmi.RemoteException{
    if (corsoService == null)
      _initCorsoServiceProxy();
    return corsoService.getCorsoById(arg0);
  }
  
  
}