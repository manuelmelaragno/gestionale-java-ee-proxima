package srv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ImpiegatoDAO;
import model.Impiegato;

/**
 * Servlet implementation class ImpiegatoServlet
 */
@WebServlet("/ImpiegatoServlet")
public class ImpiegatoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ImpiegatoServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Impiegato i = null;
		List<Impiegato> list = new ArrayList<Impiegato>();
		try {
			switch (request.getParameter("operazione")) {
			case "inserisciImpiegato":
				System.out.println("Inserisco impieagto");
				i = new Impiegato();
				if (request.getParameter("nome") != null && !request.getParameter("nome").trim().equals(""))
					i.setNome(request.getParameter("nome"));
				else
					i.setNome("");
				if (request.getParameter("cognome") != null && !request.getParameter("cognome").trim().equals(""))
					i.setCognome(request.getParameter("cognome"));
				else
					i.setCognome("");
				if (request.getParameter("email") != null && !request.getParameter("email").trim().equals(""))
					i.setEmail(request.getParameter("email"));
				else
					i.setEmail("");
				if (request.getParameter("matricola") != null && !request.getParameter("matricola").trim().equals(""))
					i.setMatricola(Integer.parseInt(request.getParameter("matricola")));
				else
					i.setMatricola(null);

				ImpiegatoDAO.insertImpiegato(i);
				request.setAttribute("message", "Impiegato inserito");
				request.getRequestDispatcher("/success.jsp").forward(request, response);
				System.out.println("Impiegato inserito");
				break;
			case "trovaImpiegato":
				switch (request.getParameter("proprieta")) {
				case "matricola":
					System.out.println("Cerco impiegato per matricola");
					i = ImpiegatoDAO.findImpiegatoByMatricola(Integer.parseInt(request.getParameter("value")));
					if (i != null) {
						list = new ArrayList<Impiegato>();
						list.add(i);
						request.setAttribute("impiegati", list);
						request.getRequestDispatcher("/impiegato/ricercaImpiegato.jsp").forward(request, response);
						System.out.println("Impiegato trovato");
					} else {
						request.setAttribute("message", "Impiegato non trovato");
						request.getRequestDispatcher("/warning.jsp").forward(request, response);
						System.out.println("Impiegato non presente in memoria");
					}
					break;
				case "nome":
					System.out.println("Cerco impiegato per nome");
					list = ImpiegatoDAO.findImpiegatoByNome(request.getParameter("value"));
					if (!list.isEmpty()) {
						list.add(i);
						request.setAttribute("impiegati", list);
						request.getRequestDispatcher("/impiegato/ricercaImpiegato.jsp").forward(request, response);
						System.out.println("Impiegato trovato");
					} else {
						request.setAttribute("message", "Impiegato non trovato");
						request.getRequestDispatcher("/warning.jsp").forward(request, response);
						System.out.println("Impiegato non presente in memoria");
					}
					break;
				case "cognome":
					System.out.println("Cerco impiegato per cognome");
					list = ImpiegatoDAO.findImpiegatoByCognome(request.getParameter("value"));
					if (!list.isEmpty()) {
						request.setAttribute("impiegati", list);
						request.getRequestDispatcher("/impiegato/ricercaImpiegato.jsp").forward(request, response);
						System.out.println("Impiegato trovato");
					} else {
						request.setAttribute("message", "Impiegato non trovato");
						request.getRequestDispatcher("/warning.jsp").forward(request, response);
						System.out.println("Impiegato non presente in memoria");
					}
					break;
				case "email":
					System.out.println("Cerco impiegato per email");
					list = ImpiegatoDAO.findImpiegatoByEmail(request.getParameter("value"));
					if (!list.isEmpty()) {
						request.setAttribute("impiegati", list);
						request.getRequestDispatcher("/impiegato/ricercaImpiegato.jsp").forward(request, response);
						System.out.println("Impiegato trovato");
					} else {
						request.setAttribute("message", "Impiegato non trovato");
						request.getRequestDispatcher("/warning.jsp").forward(request, response);
						System.out.println("Impiegato non presente in memoria");
					}
					break;
				default:
					break;
				}
				break;
			case "stampaImpiegati":
				System.out.println("Stampo impiegati");
				List<Impiegato> impiegati = new ArrayList<Impiegato>();
				impiegati = ImpiegatoDAO.getImpiegati();
				if (!impiegati.isEmpty()) {
					request.setAttribute("impiegati", impiegati);
					request.getRequestDispatcher("/impiegato/ricercaImpiegato.jsp").forward(request, response);
				} else {
					request.setAttribute("message", "Impiegati non trovati/esistenti");
					request.getRequestDispatcher("/warning.jsp").forward(request, response);
				}
				break;
			case "eliminaImpiegato":
				System.out.println("Elimino impiegato");
				if (request.getParameter("matricola") != null && !request.getParameter("matricola").trim().equals("")) {
					ImpiegatoDAO.deleteImpiegato(Integer.parseInt(request.getParameter("matricola")));
					// response.getWriter().append("Impiegato eliminato");
					request.setAttribute("message", "Impiegato eliminato");
					request.getRequestDispatcher("/success.jsp").forward(request, response);
					System.out.println("Impiegato eliminato");
				} else {
					request.setAttribute("message", "Matricola non inserita");
					request.getRequestDispatcher("/error.jsp").forward(request, response);
					System.out.println("Matricola non inserita");
				}
				break;
			case "aggiornaImpiegato":
				System.out.println("Aggiorno impiegato");
				if (request.getParameter("matricola") != null && !request.getParameter("matricola").trim().equals("")) {
					switch (request.getParameter("proprieta")) {
					case "nome":
						if (request.getParameter("value") != null && !request.getParameter("value").trim().equals("")) {
							if (ImpiegatoDAO.updateImpiegatoNome(request.getParameter("value"), Integer.parseInt(request.getParameter("matricola")))) {
								request.setAttribute("message", "Nome aggiornato");
								request.getRequestDispatcher("/success.jsp").forward(request, response);
								System.out.println("Nome aggiornato");
							} else {
								request.setAttribute("message", "Impiegato non esistente");
								request.getRequestDispatcher("/warning.jsp").forward(request, response);
								System.out.println("Impiegato non esistente");
							}
						} else {
//								response.getWriter().append("Nome non inserito");
							request.setAttribute("message", "Nome non inserito");
							request.getRequestDispatcher("/error.jsp").forward(request, response);
							System.out.println("Nome non inserito");
						}
						break;
					case "cognome":
						if (request.getParameter("value") != null && !request.getParameter("value").trim().equals("")) {
							if (ImpiegatoDAO.updateImpiegatoCognome(request.getParameter("value"), Integer.parseInt(request.getParameter("matricola")))) {
								request.setAttribute("message", "Cognome aggiornato");
								request.getRequestDispatcher("/success.jsp").forward(request, response);
								System.out.println("Cognome aggiornato");
							} else {
								request.setAttribute("message", "Impiegato non esistente");
								request.getRequestDispatcher("/warning.jsp").forward(request, response);
								System.out.println("Impiegato non esistente");
							}
						} else {
							request.setAttribute("message", "Cognome non inserito");
							request.getRequestDispatcher("/error.jsp").forward(request, response);
							System.out.println("Cognome non inserito");
						}
						break;
					case "email":
						if (request.getParameter("value") != null && !request.getParameter("value").trim().equals("")) {
							if (ImpiegatoDAO.updateImpiegatoEmail(request.getParameter("value"), Integer.parseInt(request.getParameter("matricola")))) {
								request.setAttribute("message", "Email aggiornata");
								request.getRequestDispatcher("/success.jsp").forward(request, response);
								System.out.println("Email aggiornata");
							} else {
								request.setAttribute("message", "Impiegato non esistente");
								request.getRequestDispatcher("/warning.jsp").forward(request, response);
								System.out.println("Impiegato non esistente");
							}
						} else {
							request.setAttribute("message", "Email non inserita");
							request.getRequestDispatcher("/error.jsp").forward(request, response);
							System.out.println("Email non inserita");
						}
						break;
					}
				} else {
					request.setAttribute("message", "Matricola non inserita");
					request.getRequestDispatcher("/error.jsp").forward(request, response);
					System.out.println("Matricola non inserita");
				}
				break;
			default:
				break;

			}
		} catch (ClassNotFoundException | SQLException | NamingException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
