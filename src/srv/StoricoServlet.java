package srv;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.math.NumberUtils;

import dao.StoricoDAO;
import model.Storico;

/**
 * Servlet implementation class StoricoServlet
 */
@WebServlet("/StoricoServlet")
public class StoricoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StoricoServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Storico s = null;
		List<Storico> storici = new ArrayList<Storico>();

		try {
			switch (request.getParameter("operazione")) {
			case "inserisciStorico":
				System.out.println("Inserisco storico");
				if (request.getParameter("matricola") != null && !request.getParameter("matricola").trim().equals("") && NumberUtils.isDigits(request.getParameter("matricola"))) {
					s = new Storico();
					s.setMatricola(Integer.parseInt(request.getParameter("matricola")));
					if (request.getParameter("idRuolo") != null && !request.getParameter("idRuolo").trim().equals("") && NumberUtils.isDigits(request.getParameter("idRuolo"))) {
						s.setIdRuolo(Integer.parseInt(request.getParameter("idRuolo")));
						if (request.getParameter("dataInizio") != null) {
							s.setDataInizio(new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("dataInizio")));
						} else {
							s.setDataInizio(null);
						}
						if (request.getParameter("dataFine") != null) {
							s.setDataFine(new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("dataFine")));

						} else {
							s.setDataFine(null);
						}
						if (StoricoDAO.insertStorico(s)) {
							request.setAttribute("message", "Storico inserito");
							request.getRequestDispatcher("/success.jsp").forward(request, response);
							System.out.println("Storico inserito");
						} else {
							request.setAttribute("message", "Impossibile inserire storico, controlla i dati e riprova");
							request.getRequestDispatcher("/error.jsp").forward(request, response);
							System.out.println("Impossibile inserire storico");
						}
					} else {
						request.setAttribute("message", "ID Ruolo non inserito");
						request.getRequestDispatcher("/error.jsp").forward(request, response);
						System.out.println("idRuolo non inserito");
					}
				} else {
					request.setAttribute("message", "Matricola non inserita");
					request.getRequestDispatcher("/error.jsp").forward(request, response);
					System.out.println("Matricola non inserita");
				}
				break;
			case "trovaStorico":
				switch (request.getParameter("proprieta")) {
				case "dataInizio":
					System.out.println("Cerco storico per dataInizio");

					storici = StoricoDAO.findStoriciByDataInizio(new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("value")));
					if (!storici.isEmpty()) {
						request.setAttribute("storici", storici);
						request.getRequestDispatcher("/ricercaStorico.jsp").forward(request, response);
					} else {
						request.setAttribute("message", "Storico non esistente");
						request.getRequestDispatcher("/warning.jsp").forward(request, response);
						System.out.println("Storico non esistente");
					}
					break;
				case "dataFine":
					System.out.println("Cerco storico per dataFine");

					storici = StoricoDAO.findStoriciByDataFine(new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("value")));
					if (!storici.isEmpty()) {
						request.setAttribute("storici", storici);
						request.getRequestDispatcher("/ricercaStorico.jsp").forward(request, response);
					} else {
						request.setAttribute("message", "Storico non esistente");
						request.getRequestDispatcher("/warning.jsp").forward(request, response);
						System.out.println("Storico non esistente");
					}
					break;

				default:
					break;
				}

				break;
			case "stampaStorici":
				System.out.println("Stampo storici");

				storici = StoricoDAO.getStorici();
				if (!storici.isEmpty()) {
					request.setAttribute("storici", storici);
					request.getRequestDispatcher("/ricercaStorico.jsp").forward(request, response);
					System.out.println("Storici trovati");
				} else {
					request.setAttribute("message", "Non ci sono storici salvati in memoria");
					request.getRequestDispatcher("/warning.jsp").forward(request, response);
					System.out.println("Non ci sono storici salvati in memoria");
				}

				break;
			case "eliminaStorico":
				if (request.getParameter("idStorico") != null && !request.getParameter("idStorico").trim().equals("")) {
					System.out.println("Elimino storico");
					StoricoDAO.deleteStorico(Integer.parseInt(request.getParameter("idStorico")));
					request.setAttribute("message", "Storico eliminato");
					request.getRequestDispatcher("/success.jsp").forward(request, response);
					System.out.println("Storico eliminato");
				} else {
					request.setAttribute("message", "idStorico non inserito");
					request.getRequestDispatcher("/warning.jsp").forward(request, response);
					System.out.println("idStorico non inserito");
				}
				break;
			case "modificaStorico":
				if (request.getParameter("idStorico") != null && !request.getParameter("idStorico").trim().equals("")) {
					switch (request.getParameter("proprieta")) {
					case "dataInizio":
						if (request.getParameter("value") != null && !request.getParameter("value").trim().equals("")) {
							if (StoricoDAO.updateStoricoDataInizio(Integer.parseInt(request.getParameter("idStorico")), new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("value")))) {
								request.setAttribute("message", "Storico aggiornato");
								request.getRequestDispatcher("/success.jsp").forward(request, response);
								System.out.println("Storico aggiornato");
							} else {
								request.setAttribute("message", "idStorico non inserito");
								request.getRequestDispatcher("/warning.jsp").forward(request, response);
								System.out.println("Storico non esistente");
							}
						} else {
							request.setAttribute("message", "dataInizio non inserita");
							request.getRequestDispatcher("/error.jsp").forward(request, response);
							System.out.println("dataInizio non inserita");
						}
						break;
					case "dataFine":
						if (request.getParameter("value") != null && !request.getParameter("value").trim().equals("")) {
							if (StoricoDAO.updateStoricoDataFine(Integer.parseInt(request.getParameter("idStorico")), new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("value")))) {
								request.setAttribute("message", "Storico aggiornato");
								request.getRequestDispatcher("/success.jsp").forward(request, response);
								System.out.println("Storico aggiornato");
							} else {
								request.setAttribute("message", "Storico non esistente");
								request.getRequestDispatcher("/warning.jsp").forward(request, response);
							}
						} else {
							request.setAttribute("message", "dataFine non inserita");
							request.getRequestDispatcher("/error.jsp").forward(request, response);
							System.out.println("dataFine non inserita");
						}
						break;
					default:
						break;
					}
				} else {
					request.setAttribute("message", "idStorico non inserito");
					request.getRequestDispatcher("/error.jsp").forward(request, response);
					System.out.println("idStorico non inserito");
				}
				break;
			default:
				break;
			}
		} catch (ClassNotFoundException | SQLException | NumberFormatException | ParseException | NamingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
