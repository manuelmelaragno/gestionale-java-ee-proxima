package srv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.math.NumberUtils;

import dao.RuoloDAO;
import model.Ruolo;

/**
 * Servlet implementation class RuoloServlet
 */
@WebServlet("/RuoloServlet")
public class RuoloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RuoloServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Ruolo r = null;
		List<Ruolo> ruoli = new ArrayList<Ruolo>();

		try {
			switch (request.getParameter("operazione")) {
			case "inserisciRuolo":
				System.out.println("Inserisco nuovo ruolo");
				r = new Ruolo();
				if (request.getParameter("stipendio") != null && !request.getParameter("stipendio").trim().equals("")) {
					r.setStipendio(Integer.parseInt(request.getParameter("stipendio")));
				} else {
					r.setStipendio(0);
				}
				if (request.getParameter("descr") != null && !request.getParameter("descr").trim().equals("")) {
					r.setDescrizione(request.getParameter("descr"));

					RuoloDAO.insertRuolo(r);
					request.setAttribute("message", "Ruolo inserito");
					request.getRequestDispatcher("/success.jsp").forward(request, response);
					System.out.println("Ruolo inserito");
				} else {
					request.setAttribute("message", "Descrizione vuota o non inserita");
					request.getRequestDispatcher("/error.jsp").forward(request, response);
					System.out.println("Descrizione non inserita");
				}
				break;
			case "trovaRuolo":
				switch (request.getParameter("proprieta")) {
				case "descrizione":
					ruoli = RuoloDAO.findRuoloPerDescrizione(request.getParameter("value"));
					if (!ruoli.isEmpty()) {
						request.setAttribute("ruoli", ruoli);
						request.getRequestDispatcher("/ricercaRuolo.jsp").forward(request, response);
						System.out.println("Ruolo trovato");
					} else {
						request.setAttribute("message", "Ruoli non esistenti");
						request.getRequestDispatcher("/warning.jsp").forward(request, response);
						System.out.println("Ruolo non esistente");
					}
					break;
				case "stipendio":
					if (NumberUtils.isDigits(request.getParameter("value"))) {
						ruoli = RuoloDAO.findRuoloPerStipendio(Integer.parseInt(request.getParameter("value")));
						if (!ruoli.isEmpty()) {
							request.setAttribute("ruoli", ruoli);
							request.getRequestDispatcher("/ricercaRuolo.jsp").forward(request, response);
							System.out.println("Ruoli trovati");
						} else {
							request.setAttribute("message", "Ruoli non esistenti");
							request.getRequestDispatcher("/warning.jsp").forward(request, response);
							System.out.println("Ruolo non esistente");
						}
					} else {
						request.setAttribute("message", "Valore stipendio non numerico");
						request.getRequestDispatcher("/error.jsp").forward(request, response);
						System.out.println("Valore stipendio non numerico");
					}
					break;
				}

				break;
			case "stampaRuoli":
				System.out.println("Stampo i ruoli");

				ruoli = RuoloDAO.getRuoli();
				if (!ruoli.isEmpty()) {
					request.setAttribute("ruoli", ruoli);
					request.getRequestDispatcher("/ricercaRuolo.jsp").forward(request, response);
				} else {
					request.setAttribute("message", "Non ci sono ruoli in memoria");
					request.getRequestDispatcher("/warning.jsp").forward(request, response);
					System.out.println("Non ci sono ruoli salvati in memoria");
				}

				break;
			case "eliminaRuolo":
				System.out.println("Elimino ruolo");
				if (request.getParameter("idRuolo") != null && !request.getParameter("idRuolo").trim().equals("")) {
					RuoloDAO.deleteRuolo(Integer.parseInt(request.getParameter("idRuolo")));
					request.setAttribute("message", "Ruolo eliminato");
					request.getRequestDispatcher("/success.jsp").forward(request, response);
					System.out.println("Ruolo eliminato");
				} else {
					request.setAttribute("message", "ID del ruolo non inserito");
					request.getRequestDispatcher("/error.jsp").forward(request, response);
					System.out.println("idRuolo non inserito");
				}
				break;
			case "modificaRuolo":
				if (request.getParameter("idRuolo") != null && !request.getParameter("idRuolo").trim().equals("")) {
					switch (request.getParameter("proprieta")) {
					case "stipendio":
						if (request.getParameter("value") != null && !request.getParameter("value").trim().equals("")) {
							if (NumberUtils.isDigits(request.getParameter("value"))) {
								if (RuoloDAO.updateRuoloStipendio(Integer.parseInt(request.getParameter("value")), Integer.parseInt(request.getParameter("idRuolo")))) {
									request.setAttribute("message", "Ruolo aggiornato");
									request.getRequestDispatcher("/success.jsp").forward(request, response);
									System.out.println("Ruolo aggiornato");
								} else {
									request.setAttribute("message", "Ruolo non esistente");
									request.getRequestDispatcher("/warning.jsp").forward(request, response);
									System.out.println("Ruolo non esistente");
								}
							} else {
								request.setAttribute("message", "Valore stipendio non numerico");
								request.getRequestDispatcher("/error.jsp").forward(request, response);
								System.out.println("Valore stipendio non numerico");
							}
						} else {
							request.setAttribute("message", "Valore stipendio non inserito");
							request.getRequestDispatcher("/error.jsp").forward(request, response);
							System.out.println("Valore stipendio non inserito");
						}
						break;
					case "descr":
						if (request.getParameter("value") != null && !request.getParameter("value").trim().equals("")) {
							if (RuoloDAO.updateRuoloDesc(request.getParameter("value"), Integer.parseInt(request.getParameter("idRuolo")))) {
								request.setAttribute("message", "Ruolo aggiornato");
								request.getRequestDispatcher("/success.jsp").forward(request, response);
								System.out.println("Ruolo aggiornato");
							} else {
								request.setAttribute("message", "Ruolo non esistente");
								request.getRequestDispatcher("/warning.jsp").forward(request, response);
								System.out.println("Ruolo non esistente");
							}
						} else {
							request.setAttribute("message", "Valore descrizione non inserito");
							request.getRequestDispatcher("/error.jsp").forward(request, response);
							System.out.println("Valore descrizione non inserito");
						}
						break;
					default:
						break;
					}
				} else {
					request.setAttribute("message", "ID del ruolo non inserito");
					request.getRequestDispatcher("/error.jsp").forward(request, response);
					System.out.println("idRuolo non inserito");
				}
				break;
			default:
				break;
			}
		} catch (ClassNotFoundException | SQLException | NamingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
