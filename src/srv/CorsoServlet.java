package srv;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.math.NumberUtils;
import org.jboss.resteasy.util.HttpResponseCodes;

import ws.CorsoServiceProxy;
import ws.DtoCorso;
import ws.ResponseCorsoDelete;
import ws.ResponseCorsoInsert;
import ws.ResponseCorsoSelectAll;
import ws.ResponseCorsoSelectById;

/**
 * Servlet implementation class CorsiServlet
 */
@WebServlet("/CorsoServlet")
public class CorsoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	CorsoServiceProxy corsoService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CorsoServlet() {
		super();
		corsoService = new CorsoServiceProxy();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		try {
			switch (request.getParameter("operazione")) {
			case "stampaCorsi":
				stampaCorsi(request, response);
				break;
			case "eliminaCorso":
				eliminaCorso(request, response);
				break;
			case "inserisciCorso":
				inserisciCorso(request, response);
				break;
			case "trovaCorso":

				switch (request.getParameter("proprieta")) {
				case "idCorso":
					getCorsoById(request, response);
					break;
				default:
					break;
				}

				break;
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void stampaCorsi(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseCorsoSelectAll responseCorso = new ResponseCorsoSelectAll();
		responseCorso = corsoService.getAllCorsi();
		if (responseCorso.getResponse() != null) {
			if (responseCorso.getResponse().length > 0) {
				List<DtoCorso> dtoCorsi = new ArrayList<DtoCorso>();
				for (int i = 0; i < responseCorso.getResponse().length; i++) {
					DtoCorso dtoCorso = new DtoCorso();
					dtoCorso.setNome(responseCorso.getResponse(i).getNome());
					dtoCorso.setIdCorso(responseCorso.getResponse(i).getIdCorso());
					dtoCorsi.add(dtoCorso);
				}
				request.setAttribute("corsi", dtoCorsi);
				request.getRequestDispatcher("/corsi/ricercaCorsi.jsp").forward(request, response);
			} else {
				request.setAttribute("message", "Corsi non trovati/esistenti");
				request.getRequestDispatcher("/warning.jsp").forward(request, response);
			}
		}
	}

	private void eliminaCorso(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, ServletException, IOException {
		ResponseCorsoDelete responseCorso = new ResponseCorsoDelete();
		if (request.getParameter("idCorso") != null && NumberUtils.isDigits(request.getParameter("idCorso"))) {
			responseCorso = corsoService.deleteCorso(Integer.parseInt(request.getParameter("idCorso")));
			if (responseCorso.getResponseStatus().equals("OK") && responseCorso.getResponseCode() == HttpResponseCodes.SC_OK) {
				request.setAttribute("message", "Corso eliminato");
				request.getRequestDispatcher("/success.jsp").forward(request, response);
			} else {
				request.setAttribute("message", responseCorso.getErrorDescription());
				request.getRequestDispatcher("/error.jsp").forward(request, response);
			}
		} else {
			request.setAttribute("message", "Id Corso errato");
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	private void inserisciCorso(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseCorsoInsert responseCorso = new ResponseCorsoInsert();
		if (request.getParameter("nome") != null && !request.getParameter("nome").trim().equals("")) {
			responseCorso = corsoService.insertCorso(request.getParameter("nome"));
			if (responseCorso.getResponseStatus().equals("OK") && responseCorso.getResponseCode() == HttpResponseCodes.SC_OK) {
				request.setAttribute("message", "Corso inserito");
				request.getRequestDispatcher("/success.jsp").forward(request, response);
			} else {
				request.setAttribute("message", responseCorso.getErrorDescription());
				request.getRequestDispatcher("/error.jsp").forward(request, response);
			}
		} else {
			request.setAttribute("message", "Nome errato");
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	private void getCorsoById(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseCorsoSelectById responseCorso = new ResponseCorsoSelectById();
		if (request.getParameter("value") != null && NumberUtils.isDigits(request.getParameter("value"))) {
			responseCorso = corsoService.getCorsoById(Integer.parseInt(request.getParameter("value")));
			if (responseCorso.getResponseStatus().equals("OK") && responseCorso.getResponseCode() == HttpResponseCodes.SC_OK && responseCorso.getResponse() != null) {
				List<DtoCorso> dtoCorsi = new ArrayList<DtoCorso>();
				DtoCorso dtoCorso = new DtoCorso();
				dtoCorso.setIdCorso(responseCorso.getResponse().getIdCorso());
				dtoCorso.setNome(responseCorso.getResponse().getNome());
				dtoCorsi.add(dtoCorso);

				request.setAttribute("corsi", dtoCorsi);
				request.getRequestDispatcher("/corsi/ricercaCorsi.jsp").forward(request, response);
			} else {
				request.setAttribute("message", responseCorso.getErrorDescription());
				request.getRequestDispatcher("/error.jsp").forward(request, response);
			}
		} else {
			request.setAttribute("message", "Id Corso errato");
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}
}
