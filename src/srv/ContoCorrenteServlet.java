package srv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.ContoCorrenteDTO;
import facade.ContoCorrenteEJBRemote;

/**
 * Servlet implementation class ContoCorrenteServlet
 */
@WebServlet("/ContoCorrenteServlet")
public class ContoCorrenteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(mappedName = "java:jboss/exported/Ewl_Java_EE_Proxima/ContoCorrenteEJB!facade.ContoCorrenteEJBRemote")
	private ContoCorrenteEJBRemote contoCorrenteEJB;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ContoCorrenteServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			switch (request.getParameter("operazione")) {
			case "aggiungiContoCorrente":
				inserisciConto(request, response);
				break;
			case "stampaContiCorrente":
				stampaConti(request, response);

				break;
			case "trovaConto":
				trovaConto(request, response);
				break;
			case "eliminaConto":
				deleteConto(request, response);
				break;
			case "aggiornaConto":
				System.out.println("Aggiorno Conto Corrente");
				updateConto(request, response);
				break;
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void inserisciConto(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, NamingException, ServletException, IOException {
		System.out.println("Inserisco conto corrente");
		if (request.getParameter("value") != null && !request.getParameter("value").trim().equals("")) {
			if (contoCorrenteEJB.insertContoCorrente(Integer.parseInt(request.getParameter("value")))) {
				request.setAttribute("message", "Conto corrente creato");
				request.getRequestDispatcher("/success.jsp").forward(request, response);
			} else {
				request.setAttribute("message", "Conto corrente non inserito");
				request.getRequestDispatcher("/error.jsp").forward(request, response);
			}
		}
	}

	private void deleteConto(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NumberFormatException, ClassNotFoundException, SQLException, NamingException {
		if (request.getParameter("idConto") != null && !request.getParameter("idConto").trim().equals("")) {
			System.out.println("Elimino conto corrente");
			contoCorrenteEJB.deleteContoCorrente(Integer.parseInt(request.getParameter("idConto")));
			request.setAttribute("message", "Conto corrente eliminato");
			request.getRequestDispatcher("/success.jsp").forward(request, response);
		} else {
			request.setAttribute("message", "Valore non inserito");
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	private void updateConto(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		switch (request.getParameter("proprieta")) {
		case "iban":
			if (request.getParameter("idConto") != null && !request.getParameter("idConto").trim().equals("")) {
				ContoCorrenteDTO dtoConto = new ContoCorrenteDTO();
				dtoConto.setIban(request.getParameter("value"));
				dtoConto.setIdContoCorrente(Integer.parseInt(request.getParameter("idConto")));
				contoCorrenteEJB.updateContoCorrente(dtoConto);
				request.setAttribute("message", "Conto corrente aggiornato");
				request.getRequestDispatcher("/success.jsp").forward(request, response);
			} else {
				request.setAttribute("message", "Valore non inserito");
				request.getRequestDispatcher("/error.jsp").forward(request, response);
			}
			break;
		default:
			break;
		}
	}

	private void stampaConti(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, NamingException, ServletException, IOException {
		List<ContoCorrenteDTO> contiCorrente = new ArrayList<ContoCorrenteDTO>();
		contiCorrente = new ArrayList<ContoCorrenteDTO>();
		contiCorrente = contoCorrenteEJB.getContiCorrente();
		System.out.println("Stampo conti corrente");

		if (!contiCorrente.isEmpty()) {
			request.setAttribute("conti", contiCorrente);
			request.getRequestDispatcher("/contoCorrente/ricercaConti.jsp").forward(request, response);
		} else {
			request.setAttribute("message", "Conti corrente non trovati/esistenti");
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	private void trovaConto(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, ClassNotFoundException, SQLException, NamingException, ServletException, IOException {
		List<ContoCorrenteDTO> contiCorrente = new ArrayList<ContoCorrenteDTO>();
		switch (request.getParameter("proprieta")) {
		case "idConto":
			if (request.getParameter("value") != null && !request.getParameter("value").trim().equals("")) {
				System.out.println("Trovo conto per id");
				ContoCorrenteDTO dtoContoCorrenteDTO = contoCorrenteEJB.getContoById(Integer.parseInt(request.getParameter("value")));
				contiCorrente = new ArrayList<ContoCorrenteDTO>();
				contiCorrente.add(dtoContoCorrenteDTO);

				if (dtoContoCorrenteDTO != null) {
					request.setAttribute("conti", contiCorrente);
					request.getRequestDispatcher("/contoCorrente/ricercaConti.jsp").forward(request, response);
				} else {
					request.setAttribute("message", "Conto corrente non trovato/esistente");
					request.getRequestDispatcher("/error.jsp").forward(request, response);
				}
			} else {
				request.setAttribute("message", "Valore di ricerca non inserito");
				request.getRequestDispatcher("/error.jsp").forward(request, response);
			}
			break;
		case "iban":
			if (request.getParameter("value") != null && !request.getParameter("value").trim().equals("")) {
				System.out.println("Trovo conto per iban");
				ContoCorrenteDTO dtoContoCorrente = contoCorrenteEJB.getContoByIban(request.getParameter("value"));
				contiCorrente = new ArrayList<ContoCorrenteDTO>();
				contiCorrente.add(dtoContoCorrente);

				if (dtoContoCorrente != null) {
					request.setAttribute("conti", contiCorrente);
					request.getRequestDispatcher("/contoCorrente/ricercaConti.jsp").forward(request, response);
				} else {
					request.setAttribute("message", "Conto corrente non trovato/esistente");
					request.getRequestDispatcher("/error.jsp").forward(request, response);
				}
			} else {
				request.setAttribute("message", "Valore di ricerca non inserito");
				request.getRequestDispatcher("/error.jsp").forward(request, response);
			}
			break;
		}
	}
}
