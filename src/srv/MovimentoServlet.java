package srv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.math.NumberUtils;

import dto.MovimentoDTO;
import facade.MovimentoEJBRemote;

/**
 * Servlet implementation class MovimentiServlet
 */
@WebServlet("/MovimentoServlet")
public class MovimentoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(mappedName = "java:jboss/exported/Ewl_Java_EE_Proxima/MovimentoEJB!facade.MovimentoEJBRemote")
	private MovimentoEJBRemote movimentoEJBRemote;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MovimentoServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			switch (request.getParameter("operazione")) {
			case "stampaMovimenti":
				System.out.println("Stampo tutti i movimenti");
				stampaMovimenti(request, response);
				break;
			case "eliminaMovimento":
				System.out.println("Elimino movimento");
				eliminaMovimento(request, response);
				break;
			case "trovaMovimento":
				switch (request.getParameter("proprieta")) {
				case "importo":
					System.out.println("Stampo movimenti per importo");
					getMovimentiByImporto(request, response);
					break;
				case "iban":
					System.out.println("Stampo movimenti per id cliente");
					getMovimentiByIban(request, response);
					break;
				case "idConto":
					System.out.println("Stampo movimenti per id conto");
					getMovimentiByIdConto(request, response);
					break;
				}
				break;
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("message", e.getMessage());
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void getMovimentiByIdConto(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, SQLException, NamingException {
		List<MovimentoDTO> movimenti = new ArrayList<MovimentoDTO>();

		if (request.getParameter("value") != null && !request.getParameter("value").trim().equals("") && NumberUtils.isDigits(request.getParameter("value"))) {
			movimenti = movimentoEJBRemote.getMovimentiByIdConto(Integer.parseInt(request.getParameter("value")));
			if (!movimenti.isEmpty()) {
				request.setAttribute("movimenti", movimenti);
				request.getRequestDispatcher("/movimenti/ricercaMovimenti.jsp").forward(request, response);
			} else {
				request.setAttribute("message", "Movimenti non trovati");
				request.getRequestDispatcher("/warning.jsp").forward(request, response);
			}
		} else {
			request.setAttribute("message", "IBAN errato");
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	private void getMovimentiByIban(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, SQLException, NamingException {
		List<MovimentoDTO> movimenti = new ArrayList<MovimentoDTO>();

		if (request.getParameter("value") != null && !request.getParameter("value").trim().equals("")) {
			movimenti = movimentoEJBRemote.getMovimentiByIBAN(request.getParameter("value"));
			if (!movimenti.isEmpty()) {
				request.setAttribute("movimenti", movimenti);
				request.getRequestDispatcher("/movimenti/ricercaMovimenti.jsp").forward(request, response);
			} else {
				request.setAttribute("message", "Movimenti non trovati");
				request.getRequestDispatcher("/warning.jsp").forward(request, response);
			}
		} else {
			request.setAttribute("message", "IBAN errato");
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	private void getMovimentiByImporto(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<MovimentoDTO> movimenti = new ArrayList<MovimentoDTO>();

		if (request.getParameter("value") != null && !request.getParameter("value").trim().equals("") && NumberUtils.isDigits(request.getParameter("value"))) {
			movimenti = movimentoEJBRemote.getMovimentiByImporto(Integer.parseInt(request.getParameter("value")));
			if (!movimenti.isEmpty()) {
				request.setAttribute("movimenti", movimenti);
				request.getRequestDispatcher("/movimenti/ricercaMovimenti.jsp").forward(request, response);
			} else {
				request.setAttribute("message", "Movimenti non trovati");
				request.getRequestDispatcher("/warning.jsp").forward(request, response);
			}
		} else {
			request.setAttribute("message", "Importo errato");
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	private void stampaMovimenti(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, NamingException, ServletException, IOException {
		List<MovimentoDTO> movimenti = new ArrayList<MovimentoDTO>();
		movimenti = movimentoEJBRemote.getMovimenti();
		if (!movimenti.isEmpty()) {
			request.setAttribute("movimenti", movimenti);
			request.getRequestDispatcher("/movimenti/ricercaMovimenti.jsp").forward(request, response);
		} else {
			request.setAttribute("message", "Movimenti non trovati/esistenti");
			request.getRequestDispatcher("/warning.jsp").forward(request, response);
		}
	}

	private void eliminaMovimento(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NumberFormatException, ClassNotFoundException, SQLException, NamingException {
		if (request.getParameter("idMovimento") != null && !request.getParameter("idMovimento").trim().equals("")) {
			movimentoEJBRemote.deleteMovimento(Integer.parseInt(request.getParameter("idMovimento")));
			request.setAttribute("message", "Movimento eliminato");
			request.getRequestDispatcher("/success.jsp").forward(request, response);
		} else {
			request.setAttribute("message", "ID del movimento non inserito");
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

}
