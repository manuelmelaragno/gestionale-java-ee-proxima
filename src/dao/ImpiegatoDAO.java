package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import model.Impiegato;

public class ImpiegatoDAO {

	private static Connection connectToDatabase() throws ClassNotFoundException, SQLException, NamingException {

//		String driver = "com.mysql.jdbc.Driver";
//
//		Class.forName(driver);
//
//		// Creiamo la stringa di connessione
//		String url = "jdbc:mysql://localhost:3306/gestionale_melaragno?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
//
//		// Otteniamo una connessione con username e password
//		return DriverManager.getConnection(url, "root", "1234");

		DataSource ds = (DataSource) new InitialContext().lookup("java:jboss/datasources/MySqlDS_Gestionale");
		return ds.getConnection();

	}

	public static void insertImpiegato(Impiegato impiegato)
			throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;

		dbConnection = connectToDatabase();

		String updateTableSQL = "INSERT INTO impiegato(nome, cognome, matricola, email) VALUES(?,?,?,?)";

		cmd = dbConnection.prepareStatement(updateTableSQL);

		cmd.setString(1, impiegato.getNome());
		cmd.setString(2, impiegato.getCognome());
		if (impiegato.getMatricola() != null)
			cmd.setInt(3, impiegato.getMatricola());
		else
			cmd.setString(3, null);
		cmd.setString(4, impiegato.getEmail());

		cmd.executeUpdate();

	}

	public static boolean updateImpiegatoNome(String nuovoNome, Integer matricola)
			throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		Impiegato i = null;
		boolean ret = false;

		i = findImpiegatoByMatricola(matricola);

		dbConnection = connectToDatabase();

		if (i != null) {

			String updateTableSQL = "UPDATE impiegato SET nome=? WHERE matricola=?";
			cmd = dbConnection.prepareStatement(updateTableSQL);

			cmd.setString(1, nuovoNome);
			cmd.setInt(2, matricola);

			cmd.executeUpdate();
			ret = true;

		}

		return ret;
	}

	public static boolean updateImpiegatoCognome(String nuovoCognome, Integer matricola)
			throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		Impiegato i = null;
		boolean ret = false;

		i = findImpiegatoByMatricola(matricola);

		dbConnection = connectToDatabase();

		if (i != null) {
			String updateTableSQL = "UPDATE impiegato SET cognome=? WHERE matricola=?";
			cmd = dbConnection.prepareStatement(updateTableSQL);

			cmd.setString(1, nuovoCognome);
			cmd.setInt(2, matricola);

			cmd.executeUpdate();
			ret = true;
		}

		return ret;
	}

	public static boolean updateImpiegatoEmail(String nuovaEmail, Integer matricola)
			throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		Impiegato i = null;
		boolean ret = false;

		i = findImpiegatoByMatricola(matricola);

		dbConnection = connectToDatabase();

		if (i != null) {
			String updateTableSQL = "UPDATE impiegato SET email=? WHERE matricola=?";
			cmd = dbConnection.prepareStatement(updateTableSQL);

			cmd.setString(1, nuovaEmail);
			cmd.setInt(2, matricola);

			cmd.executeUpdate();
			ret = true;
		}

		return ret;
	}

	public static void deleteImpiegato(Integer matricola) throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;

		dbConnection = connectToDatabase();

		String updateTableSQL = "DELETE FROM impiegato WHERE matricola = ?";
		cmd = dbConnection.prepareStatement(updateTableSQL);

		cmd.setInt(1, matricola);

		cmd.executeUpdate();

	}

	public static List<Impiegato> getImpiegati() throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		List<Impiegato> impiegati = new ArrayList<Impiegato>();

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM impiegato AS i";

		cmd = dbConnection.prepareStatement(selectQuery);
		ResultSet res = cmd.executeQuery();

		while (res.next()) {
			Impiegato i = new Impiegato();
			i.setNome(res.getString("NOME"));
			i.setCognome(res.getString("COGNOME"));
			i.setEmail(res.getString("EMAIL"));
			i.setMatricola(res.getInt("MATRICOLA"));
			impiegati.add(i);
		}

		return impiegati;
	}

	public static Impiegato findImpiegatoByMatricola(Integer matricola)
			throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		Impiegato i = null;

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM impiegato AS i WHERE i.matricola = ?";

		cmd = dbConnection.prepareStatement(selectQuery);
		cmd.setInt(1, matricola);

		ResultSet res = cmd.executeQuery();
		if (res.next()) {
			i = new Impiegato();
			i.setNome(res.getString("NOME"));
			i.setCognome(res.getString("COGNOME"));
			i.setEmail(res.getString("EMAIL"));
			i.setMatricola(res.getInt("MATRICOLA"));
		}

		return i;
	}

	public static List<Impiegato> findImpiegatoByNome(String nome)
			throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		Impiegato i = null;
		List<Impiegato> impiegati = new ArrayList<Impiegato>();

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM impiegato AS i WHERE i.nome = ?";

		cmd = dbConnection.prepareStatement(selectQuery);
		cmd.setString(1, nome);

		ResultSet res = cmd.executeQuery();
		while (res.next()) {
			i = new Impiegato();
			i.setNome(res.getString("NOME"));
			i.setCognome(res.getString("COGNOME"));
			i.setEmail(res.getString("EMAIL"));
			i.setMatricola(res.getInt("MATRICOLA"));

			impiegati.add(i);
		}

		return impiegati;
	}

	public static List<Impiegato> findImpiegatoByCognome(String cognome)
			throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		Impiegato i = null;
		List<Impiegato> impiegati = new ArrayList<Impiegato>();

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM impiegato AS i WHERE i.cognome = ?";

		cmd = dbConnection.prepareStatement(selectQuery);
		cmd.setString(1, cognome);

		ResultSet res = cmd.executeQuery();
		while (res.next()) {
			i = new Impiegato();
			i.setNome(res.getString("NOME"));
			i.setCognome(res.getString("COGNOME"));
			i.setEmail(res.getString("EMAIL"));
			i.setMatricola(res.getInt("MATRICOLA"));
			impiegati.add(i);
		}

		return impiegati;
	}

	public static List<Impiegato> findImpiegatoByEmail(String email)
			throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		Impiegato i = null;
		List<Impiegato> impiegati = new ArrayList<Impiegato>();

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM impiegato AS i WHERE i.email = ?";

		cmd = dbConnection.prepareStatement(selectQuery);
		cmd.setString(1, email);

		ResultSet res = cmd.executeQuery();
		while (res.next()) {
			i = new Impiegato();
			i.setNome(res.getString("NOME"));
			i.setCognome(res.getString("COGNOME"));
			i.setEmail(res.getString("EMAIL"));
			i.setMatricola(res.getInt("MATRICOLA"));
			impiegati.add(i);
		}

		return impiegati;
	}
}
