package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import model.Ruolo;

public class RuoloDAO {

	private static Connection connectToDatabase() throws ClassNotFoundException, SQLException, NamingException {

//		String driver = "com.mysql.jdbc.Driver";
//
//		Class.forName(driver);
//
//		// Creiamo la stringa di connessione
//		String url = "jdbc:mysql://localhost:3306/gestionale_melaragno?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
//
//		// Otteniamo una connessione con username e password
//		return DriverManager.getConnection(url, "root", "1234");

		DataSource ds = (DataSource) new InitialContext().lookup("java:jboss/datasources/MySqlDS_Gestionale");
		return ds.getConnection();
	}

	public static void insertRuolo(Ruolo ruolo) throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;

		dbConnection = connectToDatabase();

		String updateTableSQL = "INSERT INTO ruolo(stipendio, descr) VALUES(?,?)";

		cmd = dbConnection.prepareStatement(updateTableSQL);

		cmd.setInt(1, ruolo.getStipendio());
		cmd.setString(2, ruolo.getDescrizione());

		cmd.executeUpdate();
		System.out.println("Ruolo inserito");

	}

	public static boolean updateRuoloDesc(String nuovaDesc, Integer idRuolo)
			throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		Ruolo r = null;
		boolean ret = false;

		r = findRuoloPerId(idRuolo);

		dbConnection = connectToDatabase();

		if (r != null) {
			String updateTableSQL = "UPDATE ruolo SET descr=? WHERE id_ruolo=?";
			cmd = dbConnection.prepareStatement(updateTableSQL);

			cmd.setString(1, nuovaDesc);
			cmd.setInt(2, idRuolo);

			cmd.executeUpdate();
			ret = true;
		}

		return ret;
	}

	public static boolean updateRuoloStipendio(Integer nuovoStipendio, Integer idRuolo)
			throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		Ruolo r = null;
		boolean ret = false;

		r = findRuoloPerId(idRuolo);

		dbConnection = connectToDatabase();

		if (r != null) {
			String updateTableSQL = "UPDATE ruolo SET stipendio=? WHERE id_ruolo=?";
			cmd = dbConnection.prepareStatement(updateTableSQL);

			cmd.setInt(1, nuovoStipendio);
			cmd.setInt(2, idRuolo);

			cmd.executeUpdate();
			ret = true;
		}

		return ret;
	}

	public static void deleteRuolo(Integer idRuolo) throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;

		dbConnection = connectToDatabase();

		String updateTableSQL = "DELETE FROM ruolo WHERE id_ruolo = ?";
		cmd = dbConnection.prepareStatement(updateTableSQL);

		cmd.setInt(1, idRuolo);

		cmd.executeUpdate();

	}

	public static Ruolo findRuoloPerId(Integer idRuolo) throws SQLException, ClassNotFoundException, NamingException {
		Ruolo r = null;
		Connection dbConnection = null;
		PreparedStatement cmd = null;

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM ruolo AS r WHERE r.ID_RUOLO = ?";

		cmd = dbConnection.prepareStatement(selectQuery);
		cmd.setInt(1, idRuolo);

		ResultSet res = cmd.executeQuery();
		if (res.next()) {
			r = new Ruolo();
			r.setStipendio(Integer.parseInt(res.getString("STIPENDIO")));
			r.setDescrizione(res.getString("DESCR"));
		}

		return r;
	}

	public static List<Ruolo> findRuoloPerDescrizione(String descrizione)
			throws SQLException, ClassNotFoundException, NamingException {
		Ruolo r = null;
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		List<Ruolo> ruoli = new ArrayList<Ruolo>();

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM ruolo AS r WHERE r.DESCR = ?";

		cmd = dbConnection.prepareStatement(selectQuery);
		cmd.setString(1, descrizione);

		ResultSet res = cmd.executeQuery();
		while (res.next()) {
			r = new Ruolo();
			r.setStipendio(Integer.parseInt(res.getString("STIPENDIO")));
			r.setDescrizione(res.getString("DESCR"));

			ruoli.add(r);
		}

		return ruoli;
	}

	public static List<Ruolo> findRuoloPerStipendio(Integer stipendio)
			throws SQLException, ClassNotFoundException, NamingException {
		Ruolo r = null;
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		List<Ruolo> ruoli = new ArrayList<Ruolo>();

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM ruolo AS r WHERE r.STIPENDIO = ?";

		cmd = dbConnection.prepareStatement(selectQuery);
		cmd.setInt(1, stipendio);

		ResultSet res = cmd.executeQuery();
		while (res.next()) {
			r = new Ruolo();
			r.setStipendio(Integer.parseInt(res.getString("STIPENDIO")));
			r.setDescrizione(res.getString("DESCR"));

			ruoli.add(r);
		}

		return ruoli;
	}

	public static List<Ruolo> getRuoli() throws ClassNotFoundException, SQLException, NamingException {
		List<Ruolo> ruoli = new ArrayList<>();
		Ruolo r = null;

		Connection dbConnection = null;
		PreparedStatement cmd = null;

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM ruolo AS r";

		cmd = dbConnection.prepareStatement(selectQuery);

		ResultSet res = cmd.executeQuery();
		while (res.next()) {
			r = new Ruolo();
			r.setStipendio(Integer.parseInt(res.getString("STIPENDIO")));
			r.setDescrizione(res.getString("DESCR"));
			r.setIdRuolo(res.getInt("ID_RUOLO"));
			ruoli.add(r);
		}

		System.out.println(ruoli.toString());

		return ruoli;
	}
}
