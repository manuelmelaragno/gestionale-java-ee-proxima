package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import model.Impiegato;
import model.Ruolo;
import model.Storico;

public class StoricoDAO {
	private static Connection connectToDatabase() throws ClassNotFoundException, SQLException, NamingException {

//		String driver = "com.mysql.jdbc.Driver";
//
//		Class.forName(driver);
//
//		// Creiamo la stringa di connessione
//		String url = "jdbc:mysql://localhost:3306/gestionale_melaragno?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
//
//		// Otteniamo una connessione con username e password
//		return DriverManager.getConnection(url, "root", "1234");

		DataSource ds = (DataSource) new InitialContext().lookup("java:jboss/datasources/MySqlDS_Gestionale");
		return ds.getConnection();
	}

	public static boolean insertStorico(Storico storico) throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		boolean ret = false;
		Impiegato i = null;
		Ruolo r = null;

		i = ImpiegatoDAO.findImpiegatoByMatricola(storico.getMatricola());
		r = RuoloDAO.findRuoloPerId(storico.getIdRuolo());

		dbConnection = connectToDatabase();

		if (i != null && r != null) {

			String updateTableSQL = "INSERT INTO storico(ID_RUOLO, MATRICOLA, DATA_INIZIO, DATA_FINE) VALUES(?,?,?,?)";

			cmd = dbConnection.prepareStatement(updateTableSQL);

			cmd.setInt(1, storico.getIdRuolo());
			cmd.setInt(2, storico.getMatricola());
			if (storico.getDataInizio() != null)
				cmd.setDate(3, new java.sql.Date(storico.getDataInizio().getTime()));
			else
				cmd.setDate(3, null);
			if (storico.getDataFine() != null)
				cmd.setDate(4, new java.sql.Date(storico.getDataFine().getTime()));
			else
				cmd.setDate(4, null);

			cmd.executeUpdate();
			ret = true;
		}

		return ret;
	}

	public static boolean updateStoricoDataInizio(Integer idStorico, java.util.Date nuovaDataInizio)
			throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		Storico s = null;
		boolean ret = false;

		s = findStoricoById(idStorico);

		dbConnection = connectToDatabase();

		if (s != null) {
			String updateTableSQL = "UPDATE storico SET data_inizio=? WHERE id_storico=?";
			cmd = dbConnection.prepareStatement(updateTableSQL);

			cmd.setInt(2, idStorico);
			if (nuovaDataInizio != null)
				cmd.setDate(1, new java.sql.Date(nuovaDataInizio.getTime()));
			else
				cmd.setDate(1, null);

			cmd.executeUpdate();
			ret = true;
		}

		return ret;
	}

	public static boolean updateStoricoDataFine(Integer idStorico, java.util.Date nuovaDataFine)
			throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;
		Storico s = null;
		boolean ret = false;

		s = findStoricoById(idStorico);

		dbConnection = connectToDatabase();

		if (s != null) {
			String updateTableSQL = "UPDATE storico SET data_fine=? WHERE id_storico=?";
			cmd = dbConnection.prepareStatement(updateTableSQL);

			cmd.setInt(2, idStorico);
			if (nuovaDataFine != null)
				cmd.setDate(1, new java.sql.Date(nuovaDataFine.getTime()));
			else
				cmd.setDate(1, null);

			cmd.executeUpdate();
			ret = true;
		}

		return ret;
	}

	public static void deleteStorico(Integer idStorico) throws ClassNotFoundException, SQLException, NamingException {
		Connection dbConnection = null;
		PreparedStatement cmd = null;

		dbConnection = connectToDatabase();

		String updateTableSQL = "DELETE FROM storico WHERE id_storico = ?";
		cmd = dbConnection.prepareStatement(updateTableSQL);

		cmd.setInt(1, idStorico);

		cmd.executeUpdate();
		System.out.println("Storico eliminato");
	}

	public static Storico findStoricoById(Integer idStorico)
			throws SQLException, ClassNotFoundException, NamingException {

		Connection dbConnection = null;
		PreparedStatement cmd = null;
		Storico s = null;

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM storico WHERE id_storico = ?";

		cmd = dbConnection.prepareStatement(selectQuery);

		cmd.setInt(1, idStorico);

		ResultSet res = cmd.executeQuery();

		if (res.next()) {
			s = new Storico();

			s.setIdStorico(res.getInt("ID_STORICO"));
			s.setIdRuolo(res.getInt("ID_STORICO"));
			s.setMatricola(res.getInt("MATRICOLA"));
			s.setIdRuolo(res.getInt("ID_RUOLO"));
			s.setDataInizio(res.getDate("DATA_INIZIO"));
			s.setDataFine(res.getDate("DATA_FINE"));
		}

		return s;
	}

	public static List<Storico> getStoriciByMatricola(Integer matricola)
			throws ClassNotFoundException, SQLException, NamingException {
		List<Storico> storici = new ArrayList<Storico>();
		Storico s = null;

		Connection dbConnection = null;
		PreparedStatement cmd = null;

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM storico AS s, impiegato AS i WHERE s.matricola = i.impiegato AND i.matricola = ?";

		cmd = dbConnection.prepareStatement(selectQuery);

		cmd.setInt(0, matricola);

		ResultSet res = cmd.executeQuery();

		while (res.next()) {
			s = new Storico();
			s.setDataInizio(res.getDate("DATA_INIZIO"));
			s.setDataFine(res.getDate("DATA_FINE"));

			storici.add(s);
		}

		return storici;
	}

	// TODO_ : findStoriciByDescRuolo
	// TODO_ : findStoriciByNomeImpiegato
	// TODO_ : findStoriciByCognomeImpiegato
	// TODO_ : findStoriciByEmailImpiegato

	public static List<Storico> getStorici() throws SQLException, ClassNotFoundException, NamingException {
		List<Storico> storici = new ArrayList<>();

		Connection dbConnection = null;
		PreparedStatement cmd = null;

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM storico";

		cmd = dbConnection.prepareStatement(selectQuery);

		ResultSet res = cmd.executeQuery();

		while (res.next()) {
			Storico s = new Storico();

			s.setIdStorico(res.getInt("ID_STORICO"));
			s.setMatricola(res.getInt("MATRICOLA"));
			s.setIdRuolo(res.getInt("ID_RUOLO"));
			s.setDataInizio(res.getDate("DATA_INIZIO"));
			s.setDataFine(res.getDate("DATA_FINE"));

			storici.add(s);
		}

		return storici;
	}

	public static List<Storico> findStoriciByDataInizio(java.util.Date dataInizio)
			throws ClassNotFoundException, SQLException, NamingException {
		List<Storico> storici = new ArrayList<>();

		Connection dbConnection = null;
		PreparedStatement cmd = null;

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM storico WHERE DATA_INIZIO = ?";

		cmd = dbConnection.prepareStatement(selectQuery);

		cmd.setDate(1, new java.sql.Date(dataInizio.getTime()));

		ResultSet res = cmd.executeQuery();

		while (res.next()) {
			Storico s = new Storico();

			s.setIdStorico(res.getInt("ID_STORICO"));
			s.setMatricola(res.getInt("MATRICOLA"));
			s.setIdRuolo(res.getInt("ID_RUOLO"));
			s.setDataInizio(res.getDate("DATA_INIZIO"));
			s.setDataFine(res.getDate("DATA_FINE"));

			storici.add(s);
		}

		return storici;
	}

	public static List<Storico> findStoriciByDataFine(java.util.Date dataFine)
			throws ClassNotFoundException, SQLException, NamingException {
		List<Storico> storici = new ArrayList<>();

		Connection dbConnection = null;
		PreparedStatement cmd = null;

		dbConnection = connectToDatabase();

		String selectQuery = "SELECT * FROM storico WHERE DATA_FINE = ?";

		cmd = dbConnection.prepareStatement(selectQuery);

		cmd.setDate(1, new java.sql.Date(dataFine.getTime()));

		ResultSet res = cmd.executeQuery();

		while (res.next()) {
			Storico s = new Storico();

			s.setIdStorico(res.getInt("ID_STORICO"));
			s.setMatricola(res.getInt("MATRICOLA"));
			s.setIdRuolo(res.getInt("ID_RUOLO"));
			s.setDataInizio(res.getDate("DATA_INIZIO"));
			s.setDataFine(res.getDate("DATA_FINE"));

			storici.add(s);
		}

		return storici;
	}
}
