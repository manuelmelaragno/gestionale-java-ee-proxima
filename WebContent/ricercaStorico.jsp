<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="model.Storico"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Storici</title>
</head>
	<link rel="stylesheet" type="text/css" href="./css/w3.css">
<body style="text-align: center; background-color: #f0f0f0; padding-bottom: 10vh;">
	<div class="w3-bar w3-blue">
	  <a href="./home.jsp" class="w3-bar-item w3-hover-indigo w3-button w3-mobile">Home</a>
	</div>
<%
	@SuppressWarnings("unchecked")
	List<Storico> storici = (ArrayList<Storico>)request.getAttribute("storici");
	int count = 0;
	for(Storico s : storici){
%>
	<div class="w3-container w3-border w3-border-blue" style="background-color:white; width: 40vw; margin-left: auto; margin-right: auto; margin-top: 5vh; padding-top:0; padding-left: 0; padding-right: 0; padding-bottom: 2.5vh">
		<h2 class="w3-blue" style="color: white; padding: 2vh; margin-bottom: 5vh; margin-top: 0;">ID : <% out.print(s.getIdStorico()); %></h2>
		<p>ID Ruolo : <strong> <% out.print(s.getIdRuolo()); %> </strong></p>
		<p>Matricola : <strong><% out.print(s.getMatricola()); %></strong></p>
		<p>Data inizio : <strong><% out.print(s.getDataInizio()); %></strong></p>
		<p>Data fine : <strong><% out.print(s.getDataFine()); %></strong></p>
		<div style="text-align: right; margin-top: 5vh;">
			<form action="StoricoServlet" method="post" style="display: inline; margin-right: 2.5vh;">
				<input type="hidden" name="operazione" value="eliminaStorico">
				<input type="hidden" name="idStorico" value="<% out.print(s.getIdStorico()); %>">
				<input type="submit" class="w3-button w3-hover-indigo w3-blue" value="Elimina">
			</form>
			<a class="w3-button w3-hover-indigo w3-blue" style="margin-right: 2.5vh" href="<% out.print(request.getContextPath()); %>/modificaStorico.jsp?idStorico=<% out.print(s.getIdStorico()); %>">Modifica</a>
		</div>
		<% count++; %>
	</div>
<%
	}
%>

</body>
</html>