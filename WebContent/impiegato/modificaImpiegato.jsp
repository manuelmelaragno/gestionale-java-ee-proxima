<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifica impiegato</title>
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
</head>
<body style="padding: 0; background-color: #f0f0f0">
	<div class="w3-bar w3-blue">
	  <a href="../home.jsp" class="w3-bar-item w3-hover-indigo w3-button w3-mobile">Home</a>
	</div>
<%
	Integer matricola = Integer.parseInt(request.getParameter("matricola"));
	String nome = (String)request.getParameter("nome");
	String cognome = (String)request.getParameter("cognome");
	String email = (String)request.getParameter("email");
%>	
	<form action="<% out.print(request.getContextPath()); %>/ImpiegatoServlet" method="post">
	<div class="w3-container w3-border w3-border-blue" style="background-color: white;width: 30vw; margin-left: auto; margin-right: auto; margin-top: 15vh; padding-bottom: 5vh; padding-left: 0; padding-right: 0;">
		<input type="hidden" name="operazione" value="aggiornaImpiegato">
		<input type="hidden" name="matricola" value="<%out.print(matricola);%>">
		<h2 class="w3-blue" style="padding: 2vh; margin-bottom: 5vh; margin-top: 0;">Modifica impiegato</h2>
		<p style="font-size: 20px; padding-left: 2.5vh;">Impiegato : <strong><% out.print(nome + " " + cognome); %></strong></p>
		<p style="font-size: 20px; padding-left: 2.5vh; margin-bottom: 5vh;">E-mail : <strong><% out.print(email); %></strong></p>
		<label style="font-size: 20px; padding-left: 2.5vh;" for="proprieta">Cosa vuoi modificare?</label>
		<select name="proprieta" id="proprieta" style="font-size: 18px">
		  <option value="nome" style="font-size: 16px">Nome</option>
		  <option value="cognome" style="font-size: 16px">Cognome</option>
		  <option value="email" style="font-size: 16px">E-mail</option>
		</select>
		<br/><br/>
		<label for="value" style="font-size: 20px; padding-left: 2.5vh;">Valore</label>
		<input type="text" name="value">
		<br/> <br/>
		<input type="submit" value="Modifica" class="w3-button w3-hover-indigo w3-blue" style="display: block; margin-left: auto; margin-right: 2.5vh">
	</div>
	</form>
</body>
</html>