<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="model.Impiegato"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Impiegati</title>
	<link rel="stylesheet" type="text/css" href="./css/w3.css">
</head>
<body style="text-align: center; background-color: #f0f0f0; padding-bottom: 10vh;">
	<div class="w3-bar w3-blue">
	  <a href="../home.jsp" class="w3-bar-item w3-hover-indigo w3-button w3-mobile">Home</a>
	</div>
<%
	@SuppressWarnings("unchecked")
	List<Impiegato> impiegati = (ArrayList<Impiegato>)request.getAttribute("impiegati");
	int count = 0;
	for(Impiegato i : impiegati){
%>
	<div class="w3-container w3-border w3-border-blue" style="background-color:white; width: 40vw; margin-left: auto; margin-right: auto; margin-top: 5vh; padding-top:0; padding-left: 0; padding-right: 0; padding-bottom: 2.5vh">
		<h2 class="w3-blue" style="color: white; padding: 2vh; margin-bottom: 5vh; margin-top: 0;"><% out.print(i.getNome() + " " + i.getCognome()); %></h2>
		<p>Email : <strong><% out.print(i.getEmail()); %></strong></p>
		<p>Matricola : <strong> <% out.print(i.getMatricola()); %> </strong></p>
		<div style="text-align: right">
			<form action="ImpiegatoServlet" method="post" style="display: inline; margin-right: 2.5vh;">
				<input type="hidden" name="operazione" value="eliminaImpiegato">
				<input type="hidden" name="matricola" value="<% out.print(i.getMatricola()); %>">
				<input type="submit" class="w3-button w3-hover-indigo w3-blue" value="Elimina">
			</form>
			<a class="w3-button w3-hover-indigo w3-blue" style="margin-right: 2.5vh" href="<% out.print(request.getContextPath()); %>/impiegato/modificaImpiegato.jsp?matricola=<% out.print(i.getMatricola()); %>&nome=<% out.print(i.getNome()); %>&cognome=<% out.print(i.getCognome()); %>&email=<% out.print(i.getEmail()); %>">Modifica</a>
		</div>
		<% count++; %>
	</div>
<%
	}
%>

</body>
</html>