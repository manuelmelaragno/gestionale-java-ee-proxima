<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="./css/w3.css">
</head>
<body style="padding: 0; background-color: #f0f0f0">
	<div class="w3-bar w3-blue">
	  <a href="./home.jsp" class="w3-bar-item w3-hover-indigo w3-button w3-mobile">Home</a>
	</div>
	<% 
		String message = (String)request.getAttribute("message");
	%>
	<div class="w3-border w3-border-amber" style="background-color: white;width: 30vw; margin-left: auto; margin-right: auto; margin-top: 30vh; padding-bottom: 5vh">
		<h2  class="w3-amber" style=" margin: 0; padding-top: 2.5vh; padding-bottom: 2.5vh; color: white; text-align: center; font-weight: lighter;" >Attenzione!</h2>
		<h3 style="margin-top: 5vh; text-align: center; font-weight: lighter;"><% out.print(message); %></h3>
		
	</div>
</body>
</html>