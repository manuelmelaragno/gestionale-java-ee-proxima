<%@page import="java.util.ArrayList"%>
<%@page import="ws.DtoCorso"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Corsi</title>
<link rel="stylesheet" type="text/css" href="./css/w3.css">
</head>
<body>
	<div class="w3-bar w3-blue">
	  <a href="./home.jsp" class="w3-bar-item w3-hover-indigo w3-button w3-mobile">Home</a>
	</div>
<%
	@SuppressWarnings("unchecked")
	List<DtoCorso> dtoCorsi = (ArrayList<DtoCorso>)request.getAttribute("corsi");
	int count = 0;
	for(DtoCorso c : dtoCorsi){
%>
	<div class="w3-container w3-border w3-border-blue" style="text-align:center; background-color:white; width: 40vw; margin-left: auto; margin-right: auto; margin-top: 5vh; padding-top:0; padding-left: 0; padding-right: 0; padding-bottom: 2.5vh">
		<h2 class="w3-blue" style="color: white; padding: 2vh; margin-bottom: 5vh; margin-top: 0;"><% out.print(c.getNome()); %></h2>
		<div style="text-align: right">
			<form action="CorsoServlet" method="post" style="display: inline; margin-right: 2.5vh;">
				<input type="hidden" name="operazione" value="eliminaCorso">
				<input type="hidden" name="idCorso" value="<% out.print(c.getIdCorso()); %>">
				<input type="submit" class="w3-button w3-hover-indigo w3-blue" value="Elimina">
			</form>
			<!-- <a class="w3-button w3-hover-indigo w3-blue" style="margin-right: 2.5vh" href="<% out.print(request.getContextPath()); %>/modificaContoCorrente.jsp?idConto=<%out.print(c.getIdCorso());%>">Modifica</a> -->
		</div>
		<% count++; %>
	</div>
<%
	}
%>

</body>
</html>