<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="java.util.List"%>
    <%@page import="java.util.ArrayList"%>
    <%@page import="dto.ContoCorrenteDTO"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ricerca conti</title>
<link rel="stylesheet" type="text/css" href="./css/w3.css">
</head>
<body>
	<div class="w3-bar w3-blue">
	  <a href="../home.jsp" class="w3-bar-item w3-hover-indigo w3-button w3-mobile">Home</a>
	</div>
<%
	@SuppressWarnings("unchecked")
	List<ContoCorrenteDTO> conti = (ArrayList<ContoCorrenteDTO>)request.getAttribute("conti");
	int count = 0;
	for(ContoCorrenteDTO c : conti){ 
%>
	<div class="w3-container w3-border w3-border-blue" style="text-align:center; background-color:white; width: 40vw; margin-left: auto; margin-right: auto; margin-top: 5vh; padding-top:0; padding-left: 0; padding-right: 0; padding-bottom: 2.5vh">
		<h2 class="w3-blue" style="color: white; padding: 2vh; margin-bottom: 5vh; margin-top: 0;"><% out.print(c.getIban()); %></h2>
		<p>Saldo : <strong><% out.print(c.getSaldo()); %> €</strong></p>
		<p>Data creazione : <strong> <% out.print(c.getDataCreazione()); %> </strong></p>
		<div style="text-align: right">
			<form action="ContoCorrenteServlet" method="post" style="display: inline; margin-right: 2.5vh;">
				<input type="hidden" name="operazione" value="eliminaConto">
				<input type="hidden" name="idConto" value="<% out.print(c.getIdContoCorrente()); %>">
				<input type="submit" class="w3-button w3-hover-indigo w3-blue" value="Elimina">
			</form>
			<a class="w3-button w3-hover-indigo w3-blue" style="margin-right: 2.5vh" href="<% out.print(request.getContextPath()); %>/contoCorrente/modificaConto.jsp?idConto=<%out.print(c.getIdContoCorrente());%>&iban=<%out.print(c.getIban());%>">Modifica</a>
		</div>
		<% count++; %>
	</div>
<%
	}
%>
</body>
</html>