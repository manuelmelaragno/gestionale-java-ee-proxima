<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="model.Ruolo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ruoli</title>
	<link rel="stylesheet" type="text/css" href="./css/w3.css">
</head>
<body style="text-align: center; background-color: #f0f0f0; padding-bottom: 10vh;">
	<div class="w3-bar w3-blue">
	  <a href="./home.jsp" class="w3-bar-item w3-hover-indigo w3-button w3-mobile">Home</a>
	</div>
<%
	@SuppressWarnings("unchecked")
	List<Ruolo> ruoli = (ArrayList<Ruolo>)request.getAttribute("ruoli");
	int count = 0;
	for(Ruolo r : ruoli){
%>
	<div class="w3-container w3-border w3-border-blue" style="background-color:white; width: 40vw; margin-left: auto; margin-right: auto; margin-top: 5vh; padding-top:0; padding-left: 0; padding-right: 0; padding-bottom: 2.5vh">
		<h2 class="w3-blue" style="color: white; padding: 2vh; margin-bottom: 5vh; margin-top: 0;"><% out.print(r.getDescrizione()); %></h2>
		<p>Stipendio : <strong><% out.print(r.getStipendio()); %> &euro;</strong></p>
		<div style="text-align: right; margin-top: 5vh;">
			<form action="RuoloServlet" method="post" style="display: inline; margin-right: 2.5vh;">
				<input type="hidden" name="operazione" value="eliminaRuolo">
				<input type="hidden" name="idRuolo" value="<% out.print(r.getIdRuolo()); %>">
				<input type="submit" class="w3-button w3-hover-indigo w3-blue" value="Elimina">
			</form>
			<a class="w3-button w3-hover-indigo w3-blue" style="margin-right: 2.5vh" href="<% out.print(request.getContextPath()); %>/modificaRuolo.jsp?idRuolo=<% out.print(r.getIdRuolo()); %>">Modifica</a>
		</div>
		<% count++; %>
	</div>
<%
	}
%>

</body>
</html>