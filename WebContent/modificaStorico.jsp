<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifica ruolo</title>
	<link rel="stylesheet" type="text/css" href="./css/w3.css">
</head>
<body>
	<div class="w3-bar w3-blue">
	  <a href="./home.jsp" class="w3-bar-item w3-hover-indigo w3-button w3-mobile">Home</a>
	</div>
<%
	Integer idStorico = Integer.parseInt(request.getParameter("idStorico"));
%>	

	<form action="StoricoServlet" method="get">
	<div class="w3-container w3-border w3-border-blue" style="background-color: white;width: 30vw; margin-left: auto; margin-right: auto; margin-top: 15vh; padding-bottom: 5vh; padding-left: 0; padding-right: 0;">
		<input type="hidden" name="operazione" value="modificaStorico">
		<input type="hidden" name="idStorico" value="<%out.print(idStorico);%>">
		<h2 class="w3-blue" style="padding: 2vh; margin-bottom: 5vh; margin-top: 0;">Modifica storico</h2>
		<label style="font-size: 20px; padding-left: 2.5vh;" for="proprieta">Cosa vuoi modificare?</label>
		<select name="proprieta" id="proprieta" style="font-size: 18px">
		  <option value="dataInizio" style="font-size: 16px">Data inizio</option>
		  <option value="dataFine" style="font-size: 16px">Data fine</option>
		</select>
		<br/><br/>
		<label for="value" style="font-size: 20px; padding-left: 2.5vh;">Valore</label>
		<input type="date" name="value">
		<br/> <br/>
		<input type="submit" value="Modifica" class="w3-button w3-hover-indigo w3-blue" style="display: block; margin-left: auto; margin-right: 2.5vh">
	</div>
	</form>
</body>
</html>