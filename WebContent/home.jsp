<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Home</title>
<link rel="stylesheet" type="text/css" href="./css/w3.css">
</head>
<body style=" background-color: #f0f0f0; padding-bottom: 10vh;">
	<div class="w3-bar w3-blue">
	  <a href="./home.jsp" class="w3-bar-item w3-hover-indigo w3-button w3-mobile">Home</a>
	</div>
	<!-- CONTAINER IMPIEGATI -->
	<div class="w3-container w3-border w3-border-blue" style="background-color: white;padding-top:0; padding-left:0; padding-right:0 ;width: 50%; margin-left: auto; margin-right: auto; margin-top: 15vh; ">
		<h2 class="w3-blue" style="color: white; padding: 2vh; margin-bottom: 5vh; margin-top: 0;">Impiegati</h2>
		<div style="padding:2.5vh">
			<h3>Cosa vuoi fare?</h3>
			<a href="./impiegato/impiegato.html" class="w3-button w3-hover-indigo w3-blue w3-block" style="margin-top: 5vh">Inserisci un impiegato</a>
			<form action="ImpiegatoServlet" method="post" style="margin-top: 5vh">
				<input type="hidden" name="operazione" value="stampaImpiegati">
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" value="Stampa tutti gli impiegati">
			</form>
			<h3 style="margin-top: 5vh">Oppure ricerca impiegato</h3>
			<form action="ImpiegatoServlet" method="post" style="margin-top: 2.5vh">
				<input type="hidden" name="operazione" value="trovaImpiegato">
				<label for=proprieta>Ricerca impiegato per</label>
				<select name="proprieta" id="proprieta" style="font-size: 18px">
				  <option value="matricola" style="font-size: 16px">Matricola</option>
				  <option value="nome" style="font-size: 16px">Nome</option>
				  <option value="cognome" style="font-size: 16px">Cognome</option>
				  <option value="email" style="font-size: 16px">E-mail</option>
				</select>
				<br />
				<input type="text" name="value" class="w3-input" style="margin-top: 2.5vh" placeholder="Valore"><br />
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" style="margin-top: 2.5vh" value="Cerca">
			</form>
		</div>
	</div>
	
	<!-- CONTAINER RUOLI -->
	<div class="w3-container w3-border w3-border-blue" style="background-color: white;padding-top:0; padding-left:0; padding-right:0 ;width: 50%; margin-left: auto; margin-right: auto; margin-top: 15vh; ">
		<h2 class="w3-blue" style="color: white; padding: 2vh; margin-bottom: 5vh; margin-top: 0;">Ruoli</h2>
		<div style="padding:2.5vh">
			<h3>Cosa vuoi fare?</h3>
			<a href="./ruolo.html" class="w3-button w3-hover-indigo w3-blue w3-block" style="margin-top: 5vh">Inserisci un ruolo</a>
			<form action="RuoloServlet" method="post" style="margin-top: 5vh">
				<input type="hidden" name="operazione" value="stampaRuoli">
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" value="Stampa tutti i ruoli">
			</form>
			<h3 style="margin-top: 5vh">Oppure ricerca ruoli</h3>
			<form action="RuoloServlet" method="post" style="margin-top: 2.5vh">
				<input type="hidden" name="operazione" value="trovaRuolo">
				<label for=proprieta>Ricerca ruolo per</label>
				<select name="proprieta" id="proprieta" style="font-size: 18px">
				  <option value="descrizione" style="font-size: 16px">Descrizione</option>
				  <option value="stipendio" style="font-size: 16px">Stipendio</option>
				</select>
				<br />
				<input type="text" name="value" class="w3-input" style="margin-top: 2.5vh" placeholder="Valore"><br />
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" style="margin-top: 2.5vh" value="Cerca">
			</form>
		</div>
	</div>
	<!-- CONTAINER STORICO -->
	<div class="w3-container w3-border w3-border-blue" style="background-color: white;padding-top:0; padding-left:0; padding-right:0 ;width: 50%; margin-left: auto; margin-right: auto; margin-top: 15vh; ">
		<h2 class="w3-blue" style="color: white; padding: 2vh; margin-bottom: 5vh; margin-top: 0;">Storici</h2>
		<div style="padding:2.5vh">
			<h3>Cosa vuoi fare?</h3>
			<a href="./storico.html" class="w3-button w3-hover-indigo w3-blue w3-block" style="margin-top: 5vh">Inserisci uno storico</a>
			<form action="StoricoServlet" method="post" style="margin-top: 5vh">
				<input type="hidden" name="operazione" value="stampaStorici">
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" value="Stampa tutti gli storici">
			</form>
			<h3 style="margin-top: 5vh">Oppure ricerca storici</h3>
			<form action="StoricoServlet" method="post" style="margin-top: 2.5vh">
				<input type="hidden" name="operazione" value="trovaStorico">
				<label for=proprieta>Ricerca storico per</label>
				<select name="proprieta" id="proprieta" style="font-size: 18px">
				  <option value="dataInizio" style="font-size: 16px">Data inizio</option>
				  <option value="dataFine" style="font-size: 16px">Data fine</option>
				</select>
				<br />
				<input type="date" name="value" class="w3-input" style="margin-top: 2.5vh" placeholder="Valore"><br />
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" style="margin-top: 2.5vh" value="Cerca">
			</form>
		</div>
	</div>
	<!-- CONTAINER CONTO CORRENTE -->
	<div class="w3-container w3-border w3-border-blue" style="background-color: white;padding-top:0; padding-left:0; padding-right:0 ;width: 50%; margin-left: auto; margin-right: auto; margin-top: 15vh; ">
		<h2 class="w3-blue" style="color: white; padding: 2vh; margin-bottom: 5vh; margin-top: 0;">Conti Corrente</h2>
		<div style="padding:2.5vh">
			<h3>Cosa vuoi fare?</h3>
			<h4 style="margin-top: 5vh">Genera un conto corrente</h4>
			<form action="ContoCorrenteServlet" method="get" style="margin-top: 5vh">
				<input type="hidden" name="operazione" value="aggiungiContoCorrente">
				<label for="idCliente">Inserisci ID Cliente del conto corrente</label>
				<input type="number" name="value" class="w3-input" style="margin-top: 2.5vh" placeholder="Valore"><br />
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" value="Genera conto corrente">
			</form>
			<!-- <a href="./contoCorrente.html" class="w3-button w3-hover-indigo w3-blue w3-block" style="margin-top: 5vh">Inserisci un conto corrente</a> -->
			<form action="ContoCorrenteServlet" method="post" style="margin-top: 5vh">
				<input type="hidden" name="operazione" value="stampaContiCorrente">
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" value="Stampa tutti i conti corrente">
			</form>
			<h3 style="margin-top: 5vh">Oppure ricerca conti corrente</h3>
			<form action="ContoCorrenteServlet" method="post" style="margin-top: 2.5vh">
				<input type="hidden" name="operazione" value="trovaConto">
				<label for=proprieta>Ricerca conto corrente per</label>
				<select name="proprieta" id="proprieta" style="font-size: 18px">
				  <option value="idConto" style="font-size: 16px">ID Conto</option>
				  <option value="iban" style="font-size: 16px">IBAN</option>
				</select>
				<br />
				<input type="text" name="value" class="w3-input" style="margin-top: 2.5vh" placeholder="Valore"><br />
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" style="margin-top: 2.5vh" value="Cerca">
			</form>
		</div>
	</div>
	<!-- CONTAINER MOVIMENTI -->
	<div class="w3-container w3-border w3-border-blue" style="background-color: white;padding-top:0; padding-left:0; padding-right:0 ;width: 50%; margin-left: auto; margin-right: auto; margin-top: 15vh; ">
		<h2 class="w3-blue" style="color: white; padding: 2vh; margin-bottom: 5vh; margin-top: 0;">Movimenti</h2>
		<div style="padding:2.5vh">
			<h3>Cosa vuoi fare?</h3>
			<!-- <a href="./contoCorrente.html" class="w3-button w3-hover-indigo w3-blue w3-block" style="margin-top: 5vh">Inserisci un conto corrente</a> -->
			<form action="MovimentoServlet" method="post" style="margin-top: 5vh">
				<input type="hidden" name="operazione" value="stampaMovimenti">
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" value="Stampa tutti i movimenti">
			</form>
			<h3 style="margin-top: 5vh">Oppure ricerca movimenti</h3>
			<form action="MovimentoServlet" method="post" style="margin-top: 2.5vh">
				<input type="hidden" name="operazione" value="trovaMovimento">
				<label for=proprieta>Ricerca movimento per</label>
				<select name="proprieta" id="proprieta" style="font-size: 18px">
				  <option value="importo" style="font-size: 16px">Importo</option>
				  <option value="tipoMovimento" style="font-size: 16px" disabled="disabled">Tipo movimento</option>
				  <option value="iban" style="font-size: 16px">IBAN</option>
				  <option value="idConto" style="font-size: 16px">ID Conto</option>
				</select>
				<br />
				<input type="text" name="value" class="w3-input" style="margin-top: 2.5vh" placeholder="Valore"><br />
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" style="margin-top: 2.5vh" value="Cerca">
			</form>
		</div>
	</div>
	<!-- CONTAINER CORSI -->
	<div class="w3-container w3-border w3-border-blue" style="background-color: white;padding-top:0; padding-left:0; padding-right:0 ;width: 50%; margin-left: auto; margin-right: auto; margin-top: 15vh; ">
		<h2 class="w3-blue" style="color: white; padding: 2vh; margin-bottom: 5vh; margin-top: 0;">Corsi</h2>
		<div style="padding:2.5vh">
			<h3>Cosa vuoi fare?</h3>
			<!-- <a href="./contoCorrente.html" class="w3-button w3-hover-indigo w3-blue w3-block" style="margin-top: 5vh">Inserisci un conto corrente</a> -->
			<form action="CorsoServlet" method="post" style="margin-top: 5vh">
				<input type="hidden" name="operazione" value="stampaCorsi">
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" value="Stampa tutti i corsi">
			</form>
			<form action="CorsoServlet" method="post" style="margin-top: 5vh" >
				<input type="hidden" name="operazione" value="inserisciCorso">
				<input type="text" name="nome" class="w3-input" style="margin-top: 2.5vh" placeholder="Nome del corso"><br />
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" value="Inserisci nuovo corso">
			</form>
			<h3 style="margin-top: 5vh">Oppure ricerca corsi</h3>
			<form action="CorsoServlet" method="post" style="margin-top: 2.5vh">
				<input type="hidden" name="operazione" value="trovaCorso">
				<label for=proprieta>Ricerca corso per</label>
				<select name="proprieta" id="proprieta" style="font-size: 18px">
				  <option value="idCorso" style="font-size: 16px">ID Corso</option>
				</select>
				<br />
				<input type="text" name="value" class="w3-input" style="margin-top: 2.5vh" placeholder="Valore"><br />
				<input type="submit" class="w3-button w3-hover-indigo w3-blue w3-block" style="margin-top: 2.5vh" value="Cerca">
			</form>
		</div>
	</div>
</body>
</html>